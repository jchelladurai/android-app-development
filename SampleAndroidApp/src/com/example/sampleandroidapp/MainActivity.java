package com.example.sampleandroidapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener{

	boolean add,sub,mult,div;
	float num1 = 0;
    float num2 = 0;
    float result = 0;
    EditText edtTxt1;
    EditText edtTxt2;
    Button btnAdd;
    Button btnSub;
    Button btnMult;
    Button btnDiv;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnC;
    Button btn0;
    Button btnEql;
    TextView tvResult;
    int currentFocus = 1;
    String oper = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtTxt1 = (EditText) findViewById(R.id.edtTxt1);
//        edtTxt2 = (EditText) findViewById(R.id.edtTxt2);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSub = (Button) findViewById(R.id.btnSub);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnC = (Button) findViewById(R.id.btnC);
        btn0 = (Button) findViewById(R.id.btn0);
        btnEql = (Button) findViewById(R.id.btnEql);
        tvResult = (TextView) findViewById(R.id.tvResult);

        // set a listener
        btnAdd.setOnClickListener((OnClickListener) this);
        btnSub.setOnClickListener(this);
        btnMult.setOnClickListener(this);
        btnDiv.setOnClickListener(this);
        edtTxt1.setOnClickListener(this);
//        edtTxt2.setOnFocusChangeListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnC.setOnClickListener(this);
        btn0.setOnClickListener(this);
        btnEql.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

       
    	edtTxt1.setText(edtTxt1.getText() + "" + ((Button) v).getText());
        
        btnAdd.setOnClickListener(new View.OnClickListener() {  	
        	  @Override       	
        	  public void onClick(View v) {       	
        		  num1=Float.parseFloat(edtTxt1.getText()+"");        	
        		  add=true;        	
        		  edtTxt1.setText(null);        	
        	  }        	
        	 });
        
        btnSub.setOnClickListener(new View.OnClickListener() {          	
      	  @Override      	
      	  public void onClick(View v) {      	
      	   num1=Float.parseFloat(edtTxt1.getText()+"");      	
      	   sub=true;      	
      	   edtTxt1.setText(null);      	
      	  }      	
      	 });
        
        btnMult.setOnClickListener(new View.OnClickListener() {          	
      	  @Override      	
      	  public void onClick(View v) {      	
      	   num1=Float.parseFloat(edtTxt1.getText()+"");      	
      	   mult=true;      	
      	   edtTxt1.setText(null);      	
      	  }      	
      	 });
        
        btnDiv.setOnClickListener(new View.OnClickListener() {          	
      	  @Override      	
      	  public void onClick(View v) {      	
      	   num1=Float.parseFloat(edtTxt1.getText()+"");      	
      	   div=true;      	
      	   edtTxt1.setText(null);      	
      	  }      	
      	 });
        
        btnC.setOnClickListener(new View.OnClickListener() {          	
        	  @Override        	
        	  public void onClick(View v) {        	
        	   edtTxt1.setText(null);
        	   tvResult.setText("Enter numbers in the Editbox");
        	  }       	
        	 });
        
        
        btnEql.setOnClickListener(new View.OnClickListener() {
        	 @Override        	
        	 public void onClick(View v) {        	
        	  num2=Float.parseFloat(edtTxt1.getText()+"");        	
        	  	
        	  if (add==true) {       	
        		  result = num1+num2;
        		  edtTxt1.setText(num1+num2+"");
        		  tvResult.setText(num1 + " " + "+" + " " + num2 + " = " + result);
        		  add=false;        	
        	  	}
        	
        	  if (sub==true) {       	
          		  result = num1-num2;
        		  edtTxt1.setText(num1-num2+"");
         		  tvResult.setText(num1 + " " + "-" + " " + num2 + " = " + result);
          		  sub=false;        	
          	  	}
        	  	
        	  if (mult==true) {       	
          		  result = num1*num2;
        		  edtTxt1.setText(num1*num2+"");
          		  tvResult.setText(num1 + " " + "*" + " " + num2 + " = " + result);
          		  mult=false;        	
          	  	}
        	  	
        	  if (div==true) {       	
          		  result = num1/num2;
        		  edtTxt1.setText(num1/num2+"");
          		  tvResult.setText(num1 + " " + "/" + " " + num2 + " = " + result);
          		  div=false;        	
          	  	}
        	
        	 }
        	
        	  
        	
        	});

        // form the output line
        if (oper == "C")
        	tvResult.setText("Enter numbers in the Editbox");	
        else
        	tvResult.setText(num1 + " " + oper + " " + num2 + " = " + result);
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onFocusChange(View v, boolean hasFocus) {				//To set the focus on the Editboxes
//        // TODO Auto-generated method stub
//        switch (v.getId()) {
//        case R.id.edtTxt1:
//            if (hasFocus)
//                currentFocus = 1;
//            break;
//        case R.id.edtTxt2:
//            if (hasFocus)
//                currentFocus = 2;
//            break;
//        }
//    }

}
