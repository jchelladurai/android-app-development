package com.ptw.qe.androidapp;

import com.ptw.qe.androidapp.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

//Main activity(launch activity) displays the added content
//As of now only one content will be displayed as the values are not stored in db
public class MainActivity extends Activity {
	
	String[] contactDetails;
	

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle receiveBundle = getIntent().getExtras();
		setContentView(R.layout.activity_main);
		
		
		
	/*******************TextView should be created in xml file and the same id should be used to set the Text******************/	
//		if(getIntent().getStringExtra("value") != null) {
//			Log.i("Received Value", receiveBundle.getString("value"));
//			TextView textview = (TextView) findViewById(R.id.textout);
//			textview.setText(receiveBundle.getString("value"));
//		}
		
	/*************************************************************************************************************************/	
	
		
		
		
		/*******************TextView is created at run time programmatically********************************/
		//Checks if the bundle has values
		if(getIntent().getStringArrayExtra("value") != null) {
			contactDetails = receiveBundle.getStringArray("value");
			Log.i("Received Value", contactDetails[0]);
//			Button button = (Button) findViewById(R.id.AddContactButton);
//			button.setBottom(R.id.Main);
			
//			LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			View v = vi.inflate(R.layout.activity_main, null);
			
//			RelativeLayout.LayoutParams rLParams = 
//		    new RelativeLayout.LayoutParams(
//		    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		    rLParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
		   
//LayoutParams layoutparameters = layout.getLayoutParams();
//layoutparams.height = 10;
//layoutparams.width = 10;
			
			//Receive the existing layout
			LinearLayout layout = (LinearLayout) findViewById(R.id.ContactName);	
			
//			LinearLayout.LayoutParams layoutparameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//			layoutparameters.bottomMargin = 200;

			
			
			
//			RelativeLayout layout = (RelativeLayout) findViewById(R.id.Main);	
//			
//			RelativeLayout.LayoutParams layoutparameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
//			layoutparameters.addRule(RelativeLayout.ALIGN_PARENT_END);
		
		
			//Receive the existing contact name
			TextView existingContactName =(TextView) findViewById(R.id.Name);
			
			//Create a new Textview and set the parameters
			TextView newTextView = new TextView(this);
//			textview.setLayoutParams(findViewById(R.id.Name).getLayoutParams());
//			textview.setRight(R.id.Name);
//			textview.setTop(R.id.AddContact);
			newTextView.setLayoutParams(layout.getLayoutParams());
			newTextView.setPadding(existingContactName.getPaddingLeft(), existingContactName.getPaddingTop(), existingContactName.getPaddingRight(), existingContactName.getPaddingBottom());
			newTextView.setText(contactDetails[0]);
//			textview.setClickable(true);
			newTextView.setTextAppearance(this, android.R.style.TextAppearance_Large);
//			layout.removeView(textview1);
//			layout.addView(child, layoutparameters);
			
			//Add the created view to a layout
			layout.addView(newTextView);

			
//			View child = getLayoutInflater().inflate(R.layout.activity_main, newLayout, false); // used to inflate an existing layout, unable to do so
//			layout.addView(child, layoutparameters);
//			LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
////			LinearLayout newLayout = new LinearLayout(this);
////			newLayout.setId(10);
//		    View v= (View) mInflater.inflate(R.layout.activity_main, null);
//		TextView textView = (TextView) v.findViewById(R.id.);
//		textView.setText("your text");
			
			
			
			
			
			
			
			
			//Performs operation on clicking an element on the layout
			newTextView.setOnClickListener(new View.OnClickListener() {
				@Override
			       public void onClick(View v1) {
					//Send values to another activity 
					 Bundle sendBundle = new Bundle();
			         sendBundle.putStringArray("value", contactDetails);
			         
			         //print the value in logcat
			         Log.i("Entered String array value", contactDetails[1]);
			         Log.i("Entered String array value", contactDetails[2]);
			         
			         //Call one activity from another and send the values to another activity
			         Intent contactNumberActivity= new Intent(MainActivity.this, ContactNumber.class);
			         contactNumberActivity.putExtras(sendBundle);
			         startActivity(contactNumberActivity);

			       }
			   });
		}
		
		/****************************************************************************************************************/
		
		
		
		
		
		
		/*******************working***************************/	
		
		//to display emailId and phone number
		 TextView name =(TextView)findViewById(R.id.Name);
		 name.setOnClickListener(new View.OnClickListener() {

	       @Override
	       public void onClick(View v1) {
	          Intent contactNumberActivity= new Intent(MainActivity.this,ContactNumber.class);
	          startActivity(contactNumberActivity);

	       }
	   });
		 /***************************************************/	
				  
		
		 //to add a contact
		 Button addContact = (Button) findViewById(R.id.AddContactButton);
		 addContact.setOnClickListener(new View.OnClickListener() {

		       @Override
		       public void onClick(View v1) {
		          Intent addContactActivity= new Intent(MainActivity.this,AddContactActivity.class);
		          startActivity(addContactActivity);
		    	   

		       }
		   });
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		
		return true;
	
	}
	
	public static String valueOf(Object obj) {
	    return (obj == null) ? "null" : obj.toString();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}

