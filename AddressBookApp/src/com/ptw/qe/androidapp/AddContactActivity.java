package com.ptw.qe.androidapp;



import com.ptw.qe.androidapp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class AddContactActivity extends Activity {
	
	
	EditText contactName; 
	EditText phoneNumber;
	EditText emailAddress;
	 

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_contact);

		 //Receive the existing views in AddContactActivity layout
		 Button insertContact = (Button) findViewById(R.id.InsertContactButton);	       		
		 
		 
		 contactName = (EditText) findViewById(R.id.editTextContactName);
		 phoneNumber = (EditText) findViewById(R.id.editTextPhoneNumber);
		 emailAddress = (EditText) findViewById(R.id.editTextEmailAddress);
		 
		 //Performs the below operations on clicking insert Contact Button
		 insertContact.setOnClickListener(
				 new View.OnClickListener() {
					 public void onClick(View view) {
						 //passing the entered values in array
				         String[] contactDetails = {contactName.getText().toString(), phoneNumber.getText().toString(), emailAddress.getText().toString()};
				         
				         //creating a bundle to share the values in another activity
				         Bundle sendBundle = new Bundle();
				         sendBundle.putStringArray("value", contactDetails);
				         Log.i("Entered String array value", contactDetails[0]);
				         Log.i("Entered String array value", contactDetails[1]);
				         Log.i("Entered String array value", contactDetails[2]);
				         
				         //Call another activity
				         Intent mainActivity= new Intent(AddContactActivity.this, MainActivity.class);
				         mainActivity.putExtras(sendBundle);
				         startActivity(mainActivity);
				         					 
					 }
				 });
						 
 
		
	}
	

	 
	 @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_contact, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	
}
