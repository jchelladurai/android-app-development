package com.ptw.qe.androidapp;

import com.ptw.qe.androidapp.R;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ContactNumber extends Activity {
	String[] contactDetails;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_number);
		
		//to receive the value send from another activity
		Bundle receiveBundle = getIntent().getExtras();
		
		
 /**********Textviews are created at runtime programmatically i.e not created in layouts************/
			
		if(getIntent().getStringArrayExtra("value") != null) {							//checks if the bundle has values
			contactDetails = receiveBundle.getStringArray("value");						//receiving the bundle values
			Log.i("Received Value", contactDetails[1]);
			
			//Receiving the existing views
			TextView existingContactNumberTextView = (TextView) findViewById(R.id.ContactNumber);
			TextView existingEmailAddressTextView = (TextView) findViewById(R.id.Email);
			
			//Receiving the background layout from existing view
			Drawable background = existingContactNumberTextView.getBackground();
			
			//setting the existing view to be invisible
			existingContactNumberTextView.setVisibility(View.INVISIBLE);
			existingEmailAddressTextView.setVisibility(View.INVISIBLE);
			
			//Receiving the existing layout 
			RelativeLayout layout = (RelativeLayout) findViewById(R.id.ContactDetails);
			
			//creating a new textview and setting its parameters
			TextView textview = new TextView(this);
			textview.setPadding(18, 18, 18, 18);
			textview.setBackground(background);
			textview.setLayoutParams(existingContactNumberTextView.getLayoutParams());
			textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
			textview.setText(contactDetails[1]);
			textview.setClickable(true);
			
			//creating a new textview and setting its parameters
			TextView enteredEmailAddressTextView = new TextView(this);
			enteredEmailAddressTextView.setPadding(18, 18, 18, 18);
			enteredEmailAddressTextView.setBackground(background);
			enteredEmailAddressTextView.setLayoutParams(existingEmailAddressTextView.getLayoutParams());
			enteredEmailAddressTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
			enteredEmailAddressTextView.setText(contactDetails[2]);
			enteredEmailAddressTextView.setClickable(true);
			
			//Adding the created views to the existing layout
			layout.addView(textview);
			layout.addView(enteredEmailAddressTextView);
			setContentView(layout);
			
			
		}
		
		/*******************************************************************/
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contact_number, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
